use aw_errors::AWError;
use clap::{ArgAction, Parser};
use clap_verbosity_flag::Verbosity;

/// Settings for the Arctic Wolf Desktop
#[derive(Debug, Parser)]
pub struct Settings {
    /// Use user configuration
    #[arg(long = "no-user-config", short = 'U', action = ArgAction::SetTrue)]
    no_user_config: bool,

    /// Workspace to open
    workspace: Option<String>,

    /// User
    user: Option<String>,

    /// Password
    password: Option<String>,

    #[command(flatten)]
    verbose: Verbosity,
}

impl Settings {
    pub fn get_settings() -> Result<Settings, AWError> {
        let settings = Settings::parse();

        settings.validate()
    }

    fn validate(self) -> Result<Settings, AWError> {
        Ok(self)
    }
}

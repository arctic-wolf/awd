pub mod message;
pub mod settings;
pub mod ui;

use aw_errors::AWError;
use aw_ui::Application;

use crate::message::Message;

/// Application object for the desktop client
#[derive(Debug)]
pub struct DesktopApplication {}

impl Application for DesktopApplication {
    type Message = Message;

    /// Create a new application
    fn new() -> Result<Self, AWError>
    where
        Self: Sized,
    {
        Ok(DesktopApplication {})
    }

    /// Run the main loop for an application
    fn run(self) -> Result<i32, AWError> {
        Ok(0)
    }
}

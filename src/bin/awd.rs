use std::process::exit;

use aw_ui::Application;

use arctic_wolf_desktop::settings::Settings;
use arctic_wolf_desktop::DesktopApplication;

fn main() {
    println!(
        "{} - Arctic Fox Desktop {}",
        env!("CARGO_BIN_NAME"),
        env!("CARGO_PKG_VERSION")
    );

    let settings = match Settings::get_settings() {
        Ok(settings) => settings,
        Err(error) => panic!("{}", error.to_string()),
    };

    let application = match DesktopApplication::new() {
        Ok(application) => application,
        Err(error) => panic!("{}", error.to_string()),
    };

    match application.run() {
        Ok(status_code) => exit(status_code),
        Err(error) => panic!("{}", error.to_string()),
    }
}
